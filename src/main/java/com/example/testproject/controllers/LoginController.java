package com.example.testproject.controllers;

import com.example.testproject.forms.LoginForm;
import com.example.testproject.repository.UserRepository;
import com.example.testproject.validators.LoginValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    private static final String LOGIN_FORM = "form-login";

    @Autowired
    private LoginValidator loginValidator;

    @Autowired
    private UserRepository userRepository;


    //here we initialize the WebDataBinder which acts as a preprocessor(using loginValidator)login
    // for every request that reaches this controller
    @InitBinder(LOGIN_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(loginValidator);
    }

    @GetMapping(value = "/login")
    public String login(Model model) {
       model.addAttribute(LOGIN_FORM, new LoginForm());
        return "login";
    }

    @PostMapping(value = "/login")
    public String register(Model model,
                           @Valid @ModelAttribute(LOGIN_FORM) LoginForm loginForm,
                           BindingResult bindingResult) {


        //in this part i'll create custom errors(for invalid credentials) and add them to bindingResult
        boolean emailExists= userRepository.existsUserByEmail(loginForm.getEmail());
        boolean passwordExists= userRepository.existsUserByPassword(loginForm.getPassword());

        List<String> errors = new ArrayList<String>();

        if(!emailExists){
            ObjectError wrongEmail= new ObjectError("wrongEmail", "The e-mail field is invalid");
            bindingResult.addError(wrongEmail);
            errors.add(wrongEmail.toString());
        }
        if(!passwordExists){
            ObjectError wrongPassword= new ObjectError("wrongPassword","The password field is invalid");
            bindingResult.addError(wrongPassword);
            errors.add(wrongPassword.toString());
        }

        //bindingResult now consists of my custom errors and the ones
        // that come from the validator (if such exist in either case)
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", errors);
            return "login";
        }
        return "redirect:/user/homepage";
    }

}
