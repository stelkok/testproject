package com.example.testproject.controllers;

import com.example.testproject.domain.SimpleUser;
import com.example.testproject.mappers.UserModelToUser;
import com.example.testproject.models.UserModel;
import com.example.testproject.repository.UserRepository;
import com.example.testproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class IndexController {

    //requesting beans
    @Autowired
    private UserModelToUser userModelToUser;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;


    @GetMapping("/user/homepage")
    public String getUserHomePage(Model model){
        SimpleUser loggedUser = userService.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());

        model.addAttribute("user", loggedUser);
        return "homepage";
    }


    @GetMapping("/user/update-user/{id}")
    public String updateUser(@PathVariable Long id, Model model){
        SimpleUser userToUpdate=userService.findUserById(id);
        model.addAttribute("user", userToUpdate);
        return "update";
    }

    @PostMapping("/user/update-user/{id}")
    public String updateUser(@PathVariable Long id,UserModel model,Model model1){
        SimpleUser loggedUser = userService.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        SimpleUser errorUser=new SimpleUser();
        boolean emailExists=userRepository.existsUserByEmail(model.getEmail());

        //email is a unique field, checking for duplicate entry
        //also if a logged user changes mail it will 'bug' the DaoAuthenticationProvider
        //and redirect:/user/homepage will fail
        if((!loggedUser.getEmail().equals(model.getEmail())) &&(!emailExists)  ){
            model.setId(id);
            userService.updateUser(model);
            return"redirect:/login";
        }

        if((!loggedUser.getEmail().equals(model.getEmail())) &&(emailExists)  ){
            errorUser.setEmail(model.getEmail());
            model1.addAttribute("errorUser", errorUser);
            return "duplicate-entry-error";
        }

        model.setId(id);
        userService.updateUser(model);
        return "redirect:/user/homepage";
    }


}
