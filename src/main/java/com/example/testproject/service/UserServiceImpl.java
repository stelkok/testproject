package com.example.testproject.service;

import com.example.testproject.domain.SimpleUser;
import com.example.testproject.mappers.UserModelToUpdate;
import com.example.testproject.mappers.UserModelToUser;
import com.example.testproject.models.UserModel;
import com.example.testproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private  UserModelToUser userModelToUser;

    @Autowired
    private UserModelToUpdate userModelToUpdate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public SimpleUser registerUser(UserModel model){
       return userRepository.save(userModelToUser.modelToUser(model));
    }

    @Override
    public SimpleUser findUserById(Long id){return userRepository.findUserById(id).get();}


    @Override
    public SimpleUser updateUser(UserModel model){
          return userRepository.save(userModelToUpdate.modelToUpdate(model));
    }



    @Override
    public SimpleUser findUserByEmail(String email){return userRepository.findUserByEmail(email).get(); }

}
