package com.example.testproject.models;

public class UserModel {


    private Long id;
    private String fullName;
    private String email;
    private String phoneNumber;
    private String company;
    private String password;
    private String roleType;

    //################ Constructors #####################

    public UserModel(Long id,String fullName, String email, String phoneNumber, String company, String password, String roleType) {
        this.id=id;
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.company = company;
        this.password = password;
        this.roleType = roleType;
    }

    public UserModel(){ }

    //################ Setters N Getters ###################


    public Long getId() {  return id;  }

    public String getFullName() { return fullName;  }

    public String getEmail() { return email;  }

    public String getPhoneNumber() {return phoneNumber;  }

    public String getCompany() { return company; }

    public String getPassword() {return password;  }

    public void setId(Long id) { this.id = id; }

    public void setFullName(String fullName) { this.fullName = fullName; }

    public void setEmail(String email) { this.email = email; }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber;  }

    public void setCompany(String company) { this.company = company; }

    public void setPassword(String password) { this.password = password; }

    public String getRoleType() { return roleType; }

    public void setRoleType(String roleType) { this.roleType = roleType; }
}
