package com.example.testproject.validators;

import com.example.testproject.forms.LoginForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class LoginValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return LoginForm.class.isAssignableFrom(aClass);
    }



    @Override
    public void validate(Object target, Errors errors) {
        LoginForm loginForm = (LoginForm) target;

        if (loginForm.getEmail() == null) {
            errors.rejectValue("email", "email is empty");
        }
        if(loginForm.getPassword() == null){
            errors.rejectValue("password","password is empty");
        }

        ValidationUtils.rejectIfEmpty(errors, "email", "email.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "password.empty");
    }
}
