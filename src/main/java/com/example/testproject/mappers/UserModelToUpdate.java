package com.example.testproject.mappers;

import com.example.testproject.domain.SimpleUser;
import com.example.testproject.models.UserModel;
import com.example.testproject.repository.UserRepository;
import com.example.testproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserModelToUpdate {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    //assigning model values to object here thus keeping the UserService "cleaner"
    public SimpleUser modelToUpdate(UserModel model){
        SimpleUser userToUpdate=userRepository.findUserById(model.getId()).get();
        userToUpdate.setFullName(model.getFullName());
        userToUpdate.setEmail(model.getEmail());
        userToUpdate.setPhoneNumber(model.getPhoneNumber());
        userToUpdate.setCompany(model.getCompany());
        //to avoid hashing an already hashed password
        if(userToUpdate.getPassword().equals(model.getPassword())){
            userToUpdate.setPassword(model.getPassword());
        }else{
            userToUpdate.setPassword(passwordEncoder.encode((model.getPassword())));
        }
        userToUpdate.setRoleType("USER");
        return userToUpdate;
    }
}
