package com.example.testproject.controllers;


import com.example.testproject.domain.SimpleUser;
import com.example.testproject.models.UserModel;
import com.example.testproject.repository.UserRepository;
import com.example.testproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping(value = "/register")
    public String signup() {

        return "register";
    }

    @PostMapping(value = "/register")
    public  String signup(UserModel model, Model model1){
        SimpleUser errorUser=new SimpleUser();
        //email is a unique field, checking for duplicate entry
        boolean emailExists=userRepository.existsUserByEmail(model.getEmail());

            if(emailExists){
                errorUser.setEmail(model.getEmail());
                model1.addAttribute("errorUser", errorUser);
                return "duplicate-entry-error";
        }

        userService.registerUser(model);
        return "redirect:/login";
    }


}