package com.example.testproject.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;

    @Entity
    @Table(name="SIMPLE_USER")
    public class SimpleUser {


        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="user_id")
        private Long id;

        @Column(name="full_name", nullable = false)
        private String fullName;

        @Column(name="email", nullable = false, unique=true)
        private String email;

        @Column(name="phone_number")
        private String phoneNumber;

        @Column(name="company")
        private String company;

        @Column(name="password", nullable = false)
        private String password;


        @Column(name="role", nullable = false, columnDefinition = "VARCHAR(45)")
        private String roleType;

        //################ Constructors #####################

        public SimpleUser(Long id,String fullName, String email, String phoneNumber, String company, String password, String roleType) {
            this.id = id;
            this.fullName = fullName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.company = company;
            this.password = password;
            this.roleType = roleType;
        }

        public SimpleUser(){}


        //################ Setters N Getters ###################


        public Long getId() { return id; }

        public void setId(Long id) { this.id = id;    }

        public String getFullName() {  return fullName;   }

        public String getEmail() { return email; }

        public String getPhoneNumber() { return phoneNumber; }

        public String getCompany() { return company;}

        public String getPassword() { return password;   }

        public void setFullName(String fullName) { this.fullName = fullName;  }

        public void setEmail(String email) {  this.email = email;  }

        public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

        public void setCompany(String company) { this.company = company; }

        public void setPassword(String password) { this.password = password; }

        public String getRoleType() { return roleType;  }

        public void setRoleType(String roleType) {  this.roleType = roleType;  }
    }
