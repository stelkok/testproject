package com.example.testproject.repository;

import com.example.testproject.domain.SimpleUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<SimpleUser,Long> {


    boolean existsUserByEmail(String email);

    boolean existsUserByPassword(String password);

    Optional<SimpleUser> findUserByEmail(String email);

    Optional<SimpleUser> findUserById(Long id);
}
