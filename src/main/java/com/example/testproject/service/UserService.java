package com.example.testproject.service;

import com.example.testproject.domain.SimpleUser;
import com.example.testproject.models.UserModel;

public interface UserService {

    SimpleUser registerUser(UserModel model);

    SimpleUser updateUser(UserModel model);

    SimpleUser findUserByEmail(String email);

    SimpleUser findUserById(Long id);

}
