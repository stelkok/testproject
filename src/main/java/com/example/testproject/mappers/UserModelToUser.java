package com.example.testproject.mappers;

import com.example.testproject.domain.SimpleUser;
import com.example.testproject.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserModelToUser {

    @Autowired
    private PasswordEncoder passwordEncoder;

    //assigning model values to object here thus keeping the UserService "cleaner"
    public SimpleUser modelToUser(UserModel model){

        SimpleUser newSimpleUser = new SimpleUser();
        newSimpleUser.setFullName(model.getFullName());
        newSimpleUser.setEmail(model.getEmail());
        newSimpleUser.setPhoneNumber(model.getPhoneNumber());
        newSimpleUser.setCompany(model.getCompany());
        newSimpleUser.setPassword(passwordEncoder.encode(model.getPassword()));
        newSimpleUser.setRoleType("USER");
        return newSimpleUser;
    }
}
