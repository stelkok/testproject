jQuery(function ($) {
	$('#registration-validation').validate(
		{
		rules:
			{
				fullName: { required: true, maxlength: 50 },
				email: { required: true, email: true, maxlength: 50},
				phoneNumber: { digits: true, minlength: 10, maxlength: 10 },
				company: { maxlength: 50 },
				password: { required: true,minlength:8 },
				passwordConfirm: { required: true }
			},
		messages:
			{
				fullName: { required: 'Please enter your full name', maxlength: 'Can be up to 50 characters long'},
				email: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Can be up to 50 characters long'},
				phoneNumber: { digits: 'Phone number can only contain digits', mimlength: 'Must be 10 digits', maxlength: 'Must be 10 digits'},
				company: { maxlength: 'Can be up to 50 characters long'},
				password: { required: 'Please enter a password',mimlength: 'Must be at least 8 characters'},
				passwordConfirm:{ required: 'Please confirm your password'}
			}
		});

		$('#myForm').validate(
        		{
        		rules:
        			{
        				fullName: { required: true, maxlength: 50 },
        				email: { required: true, email: true, maxlength: 50},
        				phoneNumber: { digits: true, minlength: 10, maxlength: 10 },
        				company: { maxlength: 50 },
        				password: { required: true,minlength:8 }

        			},
        		messages:
        			{
        				fullName: { required: 'Please enter your full name', maxlength: 'Can be up to 50 characters long'},
        				email: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Can be up to 50 characters long'},
        				phoneNumber: { digits: 'Phone number can only contain digits', mimlength: 'Must be 10 digits', maxlength: 'Must be 10 digits'},
        				company: { maxlength: 'Can be up to 50 characters long'},
        				password: { required: 'Please enter a password',mimlength: 'Must be at least 8 characters'}
        			}

        		});



        //################# Password Confirmation Check ################
		$(function () {
                $("#register").click(function () {
                    var password = $("#password").val();
                    var confirmPassword = $("#passwordConfirm").val();
                    if (password != confirmPassword) {
                        alert("Passwords do not match.");
                        return false;
                    }
                    return true;
                });
            });

        //################ Checking if the user changes email during update form ##############
        $(document).ready(function(){
            var firstMail = $("#email").val();
            $("#update").click(function(){
            var secondMail = $("#email").val();
            if ((firstMail != secondMail) && (secondMail.length !=0)) {
                alert("After updating e-mail we have to redirect you at the log-in page");
                return true;
            }

            $("#myForm").submit(); // Submit the form

            });
         });

        //################ Check Password Strength##############
         $(document).ready(function(){
             $('input[type=password]').blur(function() {
                 var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
                 var firstMail = $("#password").val();
                 if(firstMail.match(decimal))
                 {
                 alert('Strong password requirements met!')
                 return true;
                 }
                 else
                 {
                 alert('Weak password..Your password must contain at least 1 special character,1 lowercase character,1 lowercase character,1 number and length longer than 8')
                 return false;
                 }
             });

      });

});

